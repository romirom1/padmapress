<nav id="sidebar-nav"><!-- Add class="sticky-sidebar-js" for auto-height sidebar -->
    <ul id="vertical-sidebar-nav" class="sf-menu">
      <li class="normal-item-pro current-menu-item">
        <a href="{{route('index')}}">
                <span class="icon-Old-TV"></span>
          Home
        </a>
      </li>
      <li class="normal-item-pro">
        <a href="{{route('video')}}">
                <span class="icon-Movie"></span>
          Video
        </a>
      </li>
      <li class="normal-item-pro">
        <a href="{{route('audio')}}">
                <span class="icon-Reel"></span>
          Audio
        </a>
      </li>
      <li class="normal-item-pro">
        <a href="{{route('katalog')}}">
                <span class="icon-Movie-Ticket"></span>
          Katalog
        </a>
      </li>

    </ul>
        <div class="clearfix"></div>
</nav>