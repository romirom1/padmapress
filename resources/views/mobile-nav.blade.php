<nav id="mobile-navigation-pro">
			
    <ul id="mobile-menu-pro">
    <li class="current-menu-item">
      <a href="{{route('index')}}">
            <span class="icon-Old-TV"></span>
        Home
      </a>
    <li>
    <li>
        <a href="{{route('video')}}">
              <span class="icon-Movie"></span>
          Video
        </a>
      </li>
    <li>
      <a href="{{route('audio')}}">
            <span class="icon-Reel"></span>
        Audio
      </a>
    </li>
    <li>
      <a href="{{route('katalog')}}">
            <span class="icon-Movie-Ticket"></span>
        Katalog
      </a>
    </li>
    </ul>
    <div class="clearfix"></div>
    
    {{-- <div id="search-mobile-nav-pro">
        <input type="text" placeholder="Search for Movies or TV Series" aria-label="Search">
    </div> --}}
    
</nav>