<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('style.css')}}">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">
		
		<link rel="stylesheet" href="{{asset('icons/fontawesome/css/fontawesome-all.min.css')}}"><!-- FontAwesome Icons -->
		<link rel="stylesheet" href="{{asset('icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css')}}"><!-- iconsmind.com Icons -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>	
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.4.0/lity.min.css">
		
		<title>Padmapress</title>
	</head>
	<body>
		<div id="sidebar-bg">
			
      <header id="videohead-pro" class="sticky-header">
			<div id="video-logo-background"><a href="dashboard-home.html"><img src="{{asset('images/logo-video-layo')}}ut.png" alt="Logo"></a></div>
			
			
			<div id="mobile-bars-icon-pro" class="noselect"><i class="fas fa-bars"></i></div>
		
		
			
			
			
			<div class="clearfix"></div>
			
			@include('mobile-nav')
			
      </header>
		
		
		
		@include('sidebar-nav')
		
		
		
		
		<main id="col-main">
			
			<div id="movie-detail-header-pro" style="background-image:url('http://via.placeholder.com/1442x775')">
				
				
				<a class="movie-detail-header-play-btn afterglow" href="//www.youtube.com/watch?v=XSGBVzeBUbk" data-lity><i class="fas fa-play"></i></a>
				

				
				<div id="movie-detail-gradient-pro"></div>
			</div><!-- close #movie-detail-header-pro -->
			
			
			<div id="movie-detail-rating">
				<div class="dashboard-container">
					<div class="row">
						<div class="col-sm">
							<h3>Judul Video</h3>
							
							
						</div>
					</div><!-- close .row -->
				</div><!-- close .dashboard-container -->
			</div><!-- close #movie-detail-rating -->
			
			<div class="dashboard-container">
				
				
				<div class="movie-details-section">
					<h2>Deskripsi</h2>
					<p>Mae Holland (Emma Watson) seizes the opportunity of a lifetime when she lands a job with the world's most powerful technology and social media company. Encouraged by the company's founder (Tom Hanks), Mae joins a groundbreaking experiment that pushes the boundaries of privacy, ethics and personal freedom. Her participation in the experiment, and every decision she makes soon starts to affect the lives and futures of her friends, family and that of humanity.</p>
				</div><!-- close .movie-details-section -->

				<div class="movie-details-section">
					<h2>Buku Terkait</h2>
					<div class="row">
						<div class="col-12 col-md-6 col-lg-4 col-xl-3">
							<div class="item-playlist-container-skrn">
								<a href="{{route('katalog-detail')}}"><img src="http://via.placeholder.com/569x524" alt="Listing"></a>
								<div class="item-playlist-text-skrn">
									<img src="{{asset('images/demo/user-6.jpg')}}" alt="User Profile">
									<h5><a href="{{route('katalog-detail')}}">Judul Buku</a></h5>
									<h6>Oleh : Penulis 2</h6>
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-playlist-container-skrn -->
						</div><!-- close .col -->
						
						<div class="col-12 col-md-6 col-lg-4 col-xl-3">
							<div class="item-playlist-container-skrn">
								<a href="{{route('katalog-detail')}}"><img src="http://via.placeholder.com/569x524" alt="Listing"></a>
								<div class="item-playlist-text-skrn">
									<img src="{{asset('images/demo/user-6.jpg')}}" alt="User Profile">
									<h5><a href="{{route('katalog-detail')}}">Judul Buku</a></h5>
									<h6>Oleh : Penulis 2</h6>
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-playlist-container-skrn -->
						</div><!-- close .col -->
						
						<div class="col-12 col-md-6 col-lg-4 col-xl-3">
							<div class="item-playlist-container-skrn">
								<a href="{{route('katalog-detail')}}"><img src="http://via.placeholder.com/569x524" alt="Listing"></a>
								<div class="item-playlist-text-skrn">
									<img src="{{asset('images/demo/user-6.jpg')}}" alt="User Profile">
									<h5><a href="{{route('katalog-detail')}}">Judul Buku</a></h5>
									<h6>Oleh : Penulis 2</h6>
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-playlist-container-skrn -->
						</div><!-- close .col -->
						
						<div class="col-12 col-md-6 col-lg-4 col-xl-3">
							<div class="item-playlist-container-skrn">
								<a href="{{route('katalog-detail')}}"><img src="http://via.placeholder.com/569x524" alt="Listing"></a>
								<div class="item-playlist-text-skrn">
									<img src="{{asset('images/demo/user-6.jpg')}}" alt="User Profile">
									<h5><a href="{{route('katalog-detail')}}">Judul Buku</a></h5>
									<h6>Oleh : Penulis 2</h6>
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-playlist-container-skrn -->
						</div><!-- close .col -->
					</div><!-- close .row -->
				</div><!-- close .movie-details-section -->

					
				<div class="movie-details-section">
					<h2>Audio Terkait</h2>
					<div class="row">
						<div class="col-12 col-md-6 col-lg-4 col-xl-6">
							<div class="item-listing-container-skrn">
								<a href="{{route('audio-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('audio-detail')}}">Judul Audio</a></h6>
							
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
			
			
						<div class="col-12 col-md-6 col-lg-4 col-xl-6">
							<div class="item-listing-container-skrn">
								<a href="{{route('audio-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('audio-detail')}}">Judul Audio</a></h6>
							
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
		
						<div class="col-12 col-md-6 col-lg-4 col-xl-6">
							<div class="item-listing-container-skrn">
								<a href="{{route('audio-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('audio-detail')}}">Judul Audio</a></h6>
							
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
					</div><!-- close .row -->
				
				</div><!-- close .movie-details-section -->
				
			</div><!-- close .dashboard-container -->
		</main>
		
		
		</div><!-- close #sidebar-bg-->
		
		<!-- Required Framework JavaScript -->
		<script src="{{asset('js/libs/jquery-3.3.1.min.js')}}"></script><!-- jQuery -->
		<script src="{{asset('js/libs/popper.min.js')}}" defer></script><!-- Bootstrap Popper/Extras JS -->
		<script src="{{asset('js/libs/bootstrap.min.js')}}" defer></script><!-- Bootstrap Main JS -->
		<!-- All JavaScript in Footer -->
		
		<!-- Additional Plugins and JavaScript -->
		<script src="{{asset('js/navigation.js')}}" defer></script><!-- Header Navigation JS Plugin -->
		<script src="{{asset('js/jquery.flexslider-min.js')}}" defer></script><!-- FlexSlider JS Plugin -->
		<script src="{{asset('js/jquery-asRange.min.js')}}" defer></script><!-- Range Slider JS Plugin -->
		<script src="{{asset('js/circle-progress.min.js')}}" defer></script><!-- Circle Progress JS Plugin -->
		<script src="{{asset('js/afterglow.min.js')}}" defer></script><!-- Video Player JS Plugin -->
		<script src="{{asset('js/script.js')}}" defer></script><!-- Custom Document Ready JS -->
		<script src="{{asset('js/script-dashboard.js')}}" defer></script><!-- Custom Document Ready for Dashboard Only JS -->
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.4.0/lity.min.js"></script>
		
		
	</body>
</html>