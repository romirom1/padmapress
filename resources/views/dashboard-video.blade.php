<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('style.css')}}">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">
		
		<link rel="stylesheet" href="{{asset('icons/fontawesome/css/fontawesome-all.min.css')}}"><!-- FontAwesome Icons -->
		<link rel="stylesheet" href="{{asset('icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css')}}"><!-- iconsmind.com Icons -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>	
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.4.0/lity.min.css">
		
		<title>Padmapress</title>
	</head>
	<body>
		<div id="sidebar-bg">
			
      <header id="videohead-pro" class="sticky-header">
			<div id="video-logo-background"><a href="dashboard-home.html"><img src="{{asset('images/logo-video-layout.png')}}" alt="Logo"></a></div>
			
			<div id="mobile-bars-icon-pro" class="noselect"><i class="fas fa-bars"></i></div>
			
			
			
			<div class="clearfix"></div>
			
			@include('mobile-nav')
			
      </header>
		
		
		@include('sidebar-nav')
	
		<main id="col-main">
			
			
			
			<div class="flexslider progression-studios-dashboard-slider">
		      <ul class="slides">
					<li class="progression_studios_animate_left">
						<div class="progression-studios-slider-dashboard-image-background" style="background-image:url(http://via.placeholder.com/1920x698);">
							<div class="progression-studios-slider-display-table">
								<div class="progression-studios-slider-vertical-align">
								
									<div class="container">
										
										{{-- <a class="progression-studios-slider-play-btn afterglow" href="#VideoLightbox-1"><i class="fas fa-play"></i></a> --}}
										
									{{-- <video id="VideoLightbox-1" poster="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg?v1" width="960" height="540">
										<source src="https://www.youtube.com/watch?v=1mFvvuikY3I" type="video/mp4">
										<source src="https://www.youtube.com/watch?v=1mFvvuikY3I" type="video/webm">
									</video> --}}
									
										<div class="progression-studios-slider-dashboard-caption-width">
											<div class="progression-studios-slider-caption-align">
												
												<h2><a href="{{route('video-detail')}}">Judul Video Youtube</a></h2>
												
												<p class="progression-studios-slider-description">Vivamus augue nisl, fringilla sit amet ligula in, pharetra tincidunt lacus. Pellentesque bibendum ultricies ante, id viverra erat molestie et. Sed in pulvinar nisi. In leo nisi, finibus et metus a, congue commodo mi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi ut bibendum tortor, maximus scelerisque felis. Vivamus vitae nulla ut orci iaculis ultrices. Vivamus a erat neque. Quisque ornare, risus eu dapibus rutrum, nunc neque lobortis nulla, a fringilla enim risus vel tellus. Proin sodales magna sit amet nulla lacinia congue. Pellentesque porttitor imperdiet risus et hendrerit.</p>
												
												<a class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow" href="//www.youtube.com/watch?v=XSGBVzeBUbk" data-lity><i class="fas fa-play"></i> Watch Video</a>
												
												<div class="clearfix"></div>      
												
											</div><!-- close .progression-studios-slider-caption-align -->
										</div><!-- close .progression-studios-slider-caption-width -->
									
									</div><!-- close .container -->
								
								</div><!-- close .progression-studios-slider-vertical-align -->
							</div><!-- close .progression-studios-slider-display-table -->
						
							<div class="progression-studios-slider-mobile-background-cover"></div>
						</div><!-- close .progression-studios-slider-image-background -->
					</li>
				</ul>
			</div><!-- close .progression-studios-slider - See /js/script.js file for options -->
			
			<ul class="dashboard-genres-pro row">
				<div id="program-prev" class="col-md-1 col-sm-1 col-3 col float-left"><button class="btn btn-info"><</button></div>
				<div id="program-next" class="col-md-1 col-sm-1 col-3 float-right"><button class="btn btn-info">></button></div>
				<div class="col-md-10 col-sm-10 col-6 float-left program-class">
				<li>
					
					<h6>Program 1</h6>
				</li>
				<li class="active">
					
					<h6>Program 2</h6>
				</li>
				<li>
					
					<h6>Program 3</h6>
				</li>
				<li>
					
					<h6>Program 4</h6>
				</li>
				<li>
					
					<h6>Program 5</h6>
				</li>
				<li>
					
					<h6>Program 6</h6>
				</li>
				<li>
					
					<h6>Program 7</h6>
				</li>
				<li>
					
					<h6>Program 8</h6>
				</li>
				<li>
					
					<h6>Program 9</h6>
				</li>
				<li>
					
					<h6>Program 10</h6>
				</li>
				<li>
					
					<h6>Program 11</h6>
				</li>
				<li>
					
					<h6>Program 12</h6>
				</li>
				<li>
					<h6>Program 13</h6>
				</li>
			</div>
			</ul>
			
			<div class="clearfix"></div>
			
				<div class="dashboard-container">
				
				<h4 class="heading-extra-margin-bottom">Program 2</h4>
				<div class="row">
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
					<div class="col-12 col-md-6 col-lg-4 col-xl-6">
						<div class="item-listing-container-skrn">
							<a href="{{route('video-detail')}}"><iframe width="100%" height="280" src="https://www.youtube.com/embed/_M_jpoEAijk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('video-detail')}}">Judul Video</a></h6>
						
								</div><!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
					
				</div><!-- close .row -->
				
				
				<ul class="page-numbers">
					<li><a class="previous page-numbers" href="#!"><i class="fas fa-chevron-left"></i></a></li>
					<li><span class="page-numbers current">1</span></li>
					<li><a class="page-numbers" href="#!">2</a></li>
					<li><a class="page-numbers" href="#!">3</a></li>
					<li><a class="page-numbers" href="#!">4</a></li>
					<li><a class="next page-numbers" href="#!"><i class="fas fa-chevron-right"></i></a></li>
				</ul>
				
						
			</div><!-- close .dashboard-container -->
		</main>
		
		
		</div><!-- close #sidebar-bg-->
		
		<!-- Required Framework JavaScript -->
		<script src="{{asset('js/libs/jquery-3.3.1.min.js')}}"></script><!-- jQuery -->
		<script src="{{asset('js/libs/popper.min.js')}}" defer></script><!-- Bootstrap Popper/Extras JS -->
		<script src="{{asset('js/libs/bootstrap.min.js')}}" defer></script><!-- Bootstrap Main JS -->
		<!-- All JavaScript in Footer -->
		
		<!-- Additional Plugins and JavaScript -->
		<script src="{{asset('js/navigation.js')}}" defer></script><!-- Header Navigation JS Plugin -->
		<script src="{{asset('js/jquery.flexslider-min.js')}}" defer></script><!-- FlexSlider JS Plugin -->
		<script src="{{asset('js/jquery-asRange.min.js')}}" defer></script><!-- Range Slider JS Plugin -->
		<script src="{{asset('js/circle-progress.min.js')}}" defer></script><!-- Circle Progress JS Plugin -->
		<script src="{{asset('js/afterglow.min.js')}}" defer></script><!-- Video Player JS Plugin -->
		<script src="{{asset('js/script.js')}}" defer></script><!-- Custom Document Ready JS -->
		<script src="{{asset('js/script-dashboard.js')}}" defer></script><!-- Custom Document Ready for Dashboard Only JS -->
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.4.0/lity.min.js"></script>
		<script>
			$('.program-class').slick({
  				autoplaySpeed: 5000,
				slidesToShow: 8,
				slidesToScroll: 1,
				prevArrow:'<button class="slick-prev  d-none"> < </button>',
				nextArrow:'<button class="slick-next d-none"> > </button>',
				responsive: [
					{
					breakpoint: 1024,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
					}
					},
					{
					breakpoint: 600,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1
					}
					},
					{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				]
			});
			$('#program-next').on('click',function(){
				$('.program-class').slick('slickNext');
			});
			$('#program-prev').on('click',function(){
				$('.program-class').slick('slickPrev');
			});
		</script>
	</body>
</html>