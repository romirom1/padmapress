<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard-index');
})->name('index');
Route::get('/video', function () {
    return view('dashboard-video');
})->name('video');
Route::get('/video/detail', function () {
    return view('detail-video');
})->name('video-detail');
Route::get('/audio', function () {
    return view('dashboard-audio');
})->name('audio');
Route::get('/audio/detail', function () {
    return view('detail-audio');
})->name('audio-detail');
Route::get('/katalog', function () {
    return view('dashboard-katalog');
})->name('katalog');
Route::get('/katalog/detail', function () {
    return view('detail-katalog');
})->name('katalog-detail');
