<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable=[
        'banner','judul','deskripsi','penulis_id'
    ];
    public function penulis()
    {
        return $this->belongsToMany('App\Penulis');
    }
}
