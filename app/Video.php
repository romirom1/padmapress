<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable=[
        'banner','url','judul','deskripsi','issue_id'
    ];

    public function issue()
    {
        return $this->hasMany('App\Issue');
    }
}
