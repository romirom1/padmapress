<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    protected $fillable=[
        'foto','nama_penulis'
    ];
}
