<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Audio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('banner_mobile',100);
            $table->string('banner_desktop',100);
            $table->string('url',100);
            $table->string('judul',100);
            $table->string('deskripsi');
            $table->unsignedBigInteger('buku_id');
            $table->timestamps();

            $table->foreign('buku_id')
                ->references('id')->on('buku')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
