<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Buku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('banner_mobile',100);
            $table->string('banner_desktop',100);
            $table->string('judul',100);
            $table->string('deskripsi');
            $table->unsignedBigInteger('penulis_id');
            $table->timestamps();

            $table->foreign('penulis_id')
                ->references('id')->on('penulis')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
