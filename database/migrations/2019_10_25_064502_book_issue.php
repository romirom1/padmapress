<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookIssue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_issue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('isu_id');
            $table->unsignedBigInteger('buku_id');
            $table->timestamps();

            $table->foreign('isu_id')
                ->references('id')->on('isu')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('buku_id')
                ->references('id')->on('buku')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_issue');
    }
}
