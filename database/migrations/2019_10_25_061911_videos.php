<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Videos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('banner_mobile',100);
            $table->string('banner_desktop',100);
            $table->string('url',100);
            $table->string('judul',100);
            $table->string('deskripsi');
            $table->unsignedBigInteger('isi_id')->nullable();
            $table->unsignedBigInteger('program_id')->nullable();
            $table->timestamps();

            $table->foreign('isi_id')
                ->references('id')->on('isu')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('program_id')
                ->references('id')->on('program')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
